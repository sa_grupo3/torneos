import React from 'react';

import './App.css';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom'

import CrearUsuario from './components/CrearUsuario/CrearUsuario';
import CrearTorneo from './components/CrearTorneo/CrearTorneo';
import EditarUsuario from './components/EditarUsuario/EditarUsuario';
import EditarTorneo from './components/EditarTorneo/EditarTorneo';
import EliminarUsuario from './components/EliminarUsuario/EliminarUsuario';
import EliminarTorneo from './components/EliminarTorneo/EliminarTorneo';
import Login from './components/Login/Login';
import Home from './components/Home/Home';
import CrearJuego from './components/CrearJuego/CrearJuego';
import EditarJuego from './components/EditarJuego/EditarJuego';
import EliminarJuego from './components/EliminarJuego/EliminarJuego';
import Torneo from './components/Torneo/Torneo';

const JUEGO_HOST = process.env.JUEGO_HOST ||"http://18.206.38.147:3002";
const TORNEOS_API_HOST = process.env.TORNEOS_API_HOST ||"http://18.206.38.147:3002";

console.log("JUEGO: " + JUEGO_HOST)
console.log("torneos: "+TORNEOS_API_HOST)
 

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>

    <Route  exact path="/usuarios/crear">
            <CrearUsuario TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
    </Route>
    <Route  exact path="/torneos/crear">
      <CrearTorneo TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
    </Route>
    <Route  exact path="/usuarios/editar">
      <EditarUsuario TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
    </Route>
    <Route path="/torneos/editar">
      <EditarTorneo TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/usuarios/eliminar">
      <EliminarUsuario TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/torneos/eliminar">
      <EliminarTorneo TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/Home">
            <Home TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/juegos/crear">
            <CrearJuego TORNEOS_API_HOST={TORNEOS_API_HOST}  JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/juegos/editar">
            <EditarJuego TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/juegos/eliminar">
            <EliminarJuego TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
          <Route path="/Login">
            <Login TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
                    <Route path='/torneos/:id' component={Torneo}  />
          <Route exact path="/">
            <Login TORNEOS_API_HOST={TORNEOS_API_HOST} JUEGO_HOST={JUEGO_HOST}/>
          </Route>
    </Switch>
      </Router>      
    </div>
  );
}

export default App;
