import React,{useState,useEffect} from 'react';
import './EliminarJuego.css';
import Header from '../Header/Header';
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';


const EliminarJuego = (props) => {
  const [juegos, setJuegos] = useState([]);
  const [juego_delete, setJuego_delete] = useState(0);


  useEffect(() => {
    if (juegos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/juegos")
        .then(res => {
          if (res.data.length == 0) {
            setJuegos([])
          } else {
            setJuegos(res.data)
          }
  })
  .catch(err => {
    console.log(err);
  });
     }
  });


  
  const onSubmit = (e) => {
    e.preventDefault(); 
    console.log(juego_delete)
    axios.delete(props.TORNEOS_API_HOST + "/juegos/"+juego_delete)
      .then(res => {
        console.log(res.data);
        axios.get(props.TORNEOS_API_HOST + "/juegos")
        .then(res => {
          if (res.data.length == 0) {
            setJuegos([])
          } else {
            setJuegos(res.data)
          }
  })
      })
      .catch(err => {
        console.log(err);
      });
  }
  

  return (
    <div className="EliminarJuego">
      <Header />
      <Container>
        <h1>Eliminar Juego</h1>
        <Form onSubmit={onSubmit}>
          <FormGroup className="text-left">
            <Label for="juego">Juego</Label>
            <Input type="select" name="juego" id="juego" onChange={e => setJuego_delete(e.target.value)}>
              {juegos.map((juego, key) => {
                return (<option value={juego._id} key={key}>{juego.nombre}</option>)
              })}
            </Input>
          </FormGroup>
      
          <Button>Eliminar</Button>
        </Form>
      </Container>
    </div>
  );
}

export default EliminarJuego;
