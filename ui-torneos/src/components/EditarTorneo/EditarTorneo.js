import React,{useEffect, useState} from 'react';
import './EditarTorneo.css';
import Header from '../Header/Header';
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

const EditarTorneo = (props) => {
  const [torneos, setTorneos] = useState([]);
  const [nombre, setNombre] = useState("");
  const [id, setId] = useState(0);

  useEffect(() => {
    if (torneos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/torneos")
        .then(res => {
          if (res.data.length == 0) {
            setTorneos([])
          } else {
            setTorneos(res.data);
          }
        })
    }
  });


  const onSubmit = e => {
    e.preventDefault();
    const torneo = {
      nombre: nombre,
    }
 axios.put(props.TORNEOS_API_HOST + "/torneos/"+id, torneo)
      .then(res => {
        console.log(res.data);
        axios.get(props.TORNEOS_API_HOST + "/torneos")
        .then(res2 => {
          setTorneos(res2.data);
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  const onChange = (e) => {
    const torneo = torneos.filter(j => j._id == e.target.value);
    console.log(torneos)
    if (torneo.length == 1) {
      setId(torneo[0]._id)
      setNombre(torneo[0].nombre);
    }
  }
    return (
      <div className="EditarTorneo">
        <Header />
      <Container >
       <h1>Editar Torneo</h1>
       <Form onSubmit={onSubmit} className="text-left">
         <Label for="torneo">Torneo</Label>
         <Input type="select" name="torneo" id="torneo" onChange={onChange}>
              {torneos.map((torneo, key) => {
                return (<option value={torneo._id} key={key}>{torneo.nombre}</option>)
              })}
        </Input>
        <FormGroup className="text-left">
          <Label for="nombre">Nombre</Label>
          <Input type="text" name="nombre" id="nombre" placeholder="Nombre" value={nombre} onChange={e=>setNombre(e.target.value)}/>
        </FormGroup>
        <Button color="success">Actualizar</Button>
      </Form>
    </Container>
      </div>
    );
  }

export default EditarTorneo;
