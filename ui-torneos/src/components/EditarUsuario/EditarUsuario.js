import React,{useState, useEffect} from 'react';
import Header from '../Header/Header';
import './EditarUsuario.css';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import axios from 'axios';

const EditarUsuario = (props) => { 
   const [correo, setCorreo] = useState("");
  const [password, setPassword] = useState("");
  const [nombres, setNombres] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [user_edit, setUser_Edit] = useState(0);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    if (users.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/usuarios/jugadores")
        .then(res => {
          if (res.data.usuario.length == 0) {
      setUsers([])
    }else{
            setUsers(res.data.usuario)
            }
  })
  .catch(err => {
    console.log(err);
  });
     }
  });
  const admin = false;
 const onSubmit = e => {
    e.preventDefault(); 
   const user = {
      id:user_edit,
      email: correo,
      nombres: nombres,
      apellidos: apellidos
    }
   if (password !== "") {
     user.password = password;
   }
   
    axios.post(props.TORNEOS_API_HOST + "/usuarios/editar", user)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  }

  const onChange = (e) => {
    const usuario = users.filter(u => u.id == e.target.value);
    if (usuario.length == 1) {
      setUser_Edit(e.target.value)
      setNombres(usuario[0].nombres);
      setApellidos(usuario[0].apellidos);
      setCorreo(usuario[0].email);
    }
  }

   return (<div>
    <Header />
     <Container >
       <h1>Editar Usuario</h1>
       <Form onSubmit={onSubmit} className="text-left">
         <Label for="user">Usuario</Label>
         <Input type="select" name="user" id="user" onChange={onChange}>
           {users.map((user, key) => {
             if (user.administrador) return (<div></div>);
                return (<option value={user.id} key={key}>{user.nombres}</option>)
              })}
        </Input>
        <FormGroup className="text-left">
          <Label for="correo">Email</Label>
          <Input type="email" name="email" id="correo" placeholder="Email" value={correo} onChange={e=>setCorreo(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="pass">Password</Label>
          <Input type="password" name="password" id="pass" placeholder="Password" value={password} onChange={e=>setPassword(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="nombres">Nombres</Label>
          <Input type="text" name="nombres" id="nombres" placeholder="Nombres" value={nombres} onChange={e=>setNombres(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="apellidos">Apellidos</Label>
          <Input type="text" name="apellidos" id="apellidos" placeholder="Apellidos" value={apellidos} onChange={e=>setApellidos(e.target.value)}/>
        </FormGroup>
        <Button color="success">Registrar</Button>
      </Form>
    </Container>
      
  </div>);
}

export default EditarUsuario;
