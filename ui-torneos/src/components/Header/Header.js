import React, { useState} from 'react';
import PropTypes from 'prop-types';
import './Header.css';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Container
} from 'reactstrap';
import { withRouter } from 'react-router-dom'; 
import Login from '../Login/Login';

const Header = (props) => {
      const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  if (localStorage.getItem("logged")===false) {
    localStorage.setItem("logged",false)
    props.history.push('/Login');      
  }

  const onClick = () => {
    localStorage.setItem("logged", false);
    props.history.push('/');
  }

    return (
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/Home">Torneos</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Usuarios
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem >
                    <NavLink href="/usuarios/crear">Crear Usuario</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/usuarios/editar">Editar Usuario</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/usuarios/eliminar">Eliminar Usuario</NavLink>
                </DropdownItem>
              </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Torneos
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink href="/torneos/crear">Crear Torneo</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/torneos/editar">Editar Torneo</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/torneos/eliminar">Eliminar Torneo</NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Juegos
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <NavLink href="/juegos/crear">Crear Juego</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/juegos/editar">Editar Juego</NavLink>
                </DropdownItem>
                <DropdownItem>
                  <NavLink href="/juegos/eliminar">Eliminar Juego</NavLink>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          
              <NavLink onClick={onClick} >Cerrar Sesion</NavLink>
            
        </Collapse>
      </Navbar>
    
  );
};

export default withRouter(Header);
