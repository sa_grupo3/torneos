import React,{useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import './Torneo.css';
import axios from 'axios';
import Header from '../Header/Header';
import { Button, Container, Table } from 'reactstrap';
const TORNEOS_API_HOST = process.env.TORNEOS_API_HOST || "http://18.206.38.147:3002";
const JUEGO_HOST = process.env.JUEGO_HOST ||"http://18.206.38.147:8000";

const NombreFase = ({ fase }) => {
  switch (fase) {
    case "1":
      return"Final"
    case "2":
      return "Semis"
    case "4":
      return"Cuartos"
    case "8":
      return "Octavos"
    case "16":
      return "16vos"
    case "32":
      return "32vos"
    case "64":
      return "64vos"
    case "128":
      return"128vos"
    default:
      return"Final"
  }
}

  const RenderPartida = (props) => {
    const [partida, setPartida] = useState("")
    const [jugador1, setJugador1] = useState("");
    const [jugador2, setJugador2] = useState("");
    const [token, setToken] = useState({})
    


async function GetToken() {
  var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";

  var token2 = await axios.post(`${token_url}/token?id=craps&secret=manager1`, {})
    .then(function (response) {
      var tk = response.data
      return tk;
})
.catch(function (error) {
  console.log(error);
});
  return token2;
}


    const Simular = async (e) => {
   const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
  
      
      const s = await axios.post(JUEGO_HOST + '/simular', { id: partida.partida.id, jugadores: partida.partida.jugadores },config);
      if (s.status == 201) {
        alert('Partida Simulada');
      const t = await axios.get(TORNEOS_API_HOST + '/partidas/' + props.llave);
      setPartida(t.data)
      } else if(s.status==404){
        alert('Jugador no encontrado');
      }else if(s.status==406){
        alert('Parametros no validos');
      }
    }

    useEffect(() => {
    async function fetchData(){
      const t = await axios.get(TORNEOS_API_HOST + '/partidas/' + props.llave);
      setPartida(t.data)
      const jg1 = await axios.get(TORNEOS_API_HOST + '/usuarios/jugadores/' + t.data.partida.jugadores[0]);
      const jg2 = await axios.get(TORNEOS_API_HOST + '/usuarios/jugadores/' + t.data.partida.jugadores[1]);
      setJugador1(jg1.data)
      setJugador2(jg2.data)
      
    }
    if(partida=="")fetchData();
  });

    
    if (partida !== "") {
      var BtnDisable = false;
      if (partida.partida.hasOwnProperty('marcador')&&(partida.partida.marcador[0]==0&&partida.partida.marcador[1]==0)) BtnDisable = true;
   //   console.log(partida)
      return (<tr>
        <td>{jugador1.hasOwnProperty('usuario') ? jugador1.usuario.nombres : ""}</td>
        <td>{jugador2.hasOwnProperty('usuario') ? jugador2.usuario.nombres : ""}</td>
        <td>{partida.partida.hasOwnProperty('marcador')&&partida.partida.marcador.length==2 ? partida.partida.marcador[0]+" - "+partida.partida.marcador[1] : "0 - 0"}</td>
        <td><Button disabled={BtnDisable} onClick={Simular}>Simular</Button></td>
      </tr>)
    }else return <tr></tr>
    
  }



const Torneo = ({ match}) => {
  const [torn, setTorn] = useState("");
  const [ganador, setGanador] = useState("");
  
  useEffect(() => {
    async function fetchData(){
      const t = await axios.get(TORNEOS_API_HOST + '/torneos/' + match.params.id);
      console.log(t.data)
      setTorn(t.data)
      /*
      */
      if (t.data.hasOwnProperty('abierto')&&!t.data.abierto) {
        const gan = await axios.get(TORNEOS_API_HOST + '/usuarios/jugadores/' + t.data.ganador)
        setGanador(gan.data.usuario.nombres)
      }
    }
    
    
    if(torn=="")fetchData();
  });


  const GetLlaves = ({ llaves }) => {
    return (
      <tr>
        <td>
          {<NombreFase fase={llaves.nombre}></NombreFase>}
        </td>
        <td>
          
          <Table responsive striped>
            <thead>
              <tr>
                <th>
                  Jugador 1
              </th>
                <th>
                  Jugador 2
              </th>
                <th>
                  Resultado
              </th>
                <th>
                </th>
              </tr>
            </thead>
            <tbody>
              {llaves.llaves.map(llave => {
                
                return <RenderPartida llave={llave} key={llave}/>
                
              })}
            </tbody>
          </Table>
        </td>
      </tr>
    )
   }
   
  return (
    <div>
      <Header />
      <Container>
        <h1>
          {torn.nombre}
        </h1>
        <h2> {torn.hasOwnProperty('abierto')&&!torn.abierto?"¡Felicidades "+ ganador+"!":""}</h2>
        <Table responsive striped hover >
          <thead>
            <tr>
              <th>
                Fase
              </th>
              <th>
                LLaves
              </th>
            </tr>
          </thead>
          <tbody>
            {torn.hasOwnProperty("fase")?torn.fase.map((fas,key) => {
              return (<GetLlaves llaves={fas} key={key}/>)
            }):<tr></tr>
            }
          </tbody>
        </Table>
      </Container>
            </div>
            /*<DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}/>*/

            );
    };

const TorneoDetail = (props) => {
    
}

Torneo.propTypes = {};

Torneo.defaultProps = {};

export default Torneo;
