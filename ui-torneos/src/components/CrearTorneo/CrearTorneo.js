import React,{useState,useEffect} from 'react';
import './CrearTorneo.css';
import Header from '../Header/Header';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import axios from 'axios';
import MultiSelect from "react-multi-select-component";
const CrearTorneo = (props) => {
  const [nombre, setNombre] = useState("");
  const [juego, setJuego] = useState(0);
  const [juegos, setJuegos] = useState([]);
  const [nojugadores, setNoJugadores] = useState(0);
  const [usuarios,setUsuarios] = useState([])
  const [selected, setSelected] = useState([]);
  const [usuarios_o, setUsuarios_O] = useState([]);

  useEffect(() => {
    if (juegos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/juegos")
        .then(res => {
          if (res.data.length == 0) {
            setJuegos([])
          } else { 
          setJuegos(res.data);
        }
        })
    }
      if (usuarios.length == 0) { 
             axios.get(props.TORNEOS_API_HOST + "/usuarios/jugadores")
        .then(res => {
          if (res.data.length == 0) {
            setUsuarios([])
          } else { 
            var users = [];
            res.data.usuario.forEach(u => {
              if (!u.administrador) {
              users.push({
                label: u.nombres,
                value: u.id
              })
                }
            });
            setUsuarios(users);
            setUsuarios_O(res.data.usuario);
        }
        })
      }
  });

    const onSubmit = e => {
    e.preventDefault(); 
      
      if (nombre !== "" && juego !== 0 && nojugadores !== 0 && selected.length > 0) {
        var jugadores = [];
        selected.forEach(o => {
          jugadores.push(usuarios_o.filter(u=>u.id===o.value)[0].id)
        })
        console.log(jugadores)
        if (nojugadores == jugadores.length) {
          const torneo = {
      nombre: nombre,
      juego: juegos.filter(j => j._id == juego)[0],
      nojugadores: nojugadores,
      jugadores: jugadores
        }    
        
    axios.post(props.TORNEOS_API_HOST + "/torneos", torneo)
      .then(res => {
        if (res.status == 200) {
        alert("Se creo correctamente el Torneo");  
        }
        else {
          alert("No se ha podido crear el Torneo")
        }
        
      })
      .catch(err => {
        console.log(err);
      });   
        } else {
          alert("El numero de usuarios no coincide con la cantidad de jugadores")
         }
    
      } else {
        alert("llene todos los campos")
      }
    
  }

  

  return (
    <div className="CrearTorneo">
      <Header />
      <h1>Crear Torneo</h1>
     <Container>
      <Form onSubmit={onSubmit}>
        <FormGroup className="text-left">
          <Label for="nombre">Nombre</Label>
          <Input type="text" name="nombre" id="nombre" placeholder="Nombre" value={nombre} onChange={e=>setNombre(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="juego">Juego</Label>
           <Input type="select" name="juego" id="juego" onChange={e=>setJuego(e.target.value)}>
              {juegos.map((juego, key) => {
                return (<option value={juego._id} key={key}>{juego.nombre+" - "+juego.ip}</option>)
              })}
        </Input>
          </FormGroup>
          <FormGroup className="text-left">
          <Label for="noplayers">Numero de Jugadores</Label>
           <Input type="select" name="noplayers" value={nojugadores} id="noplayers" onChange={e=>setNoJugadores(e.target.value)}>
          <option value={2}>2</option>
          <option value={4}>4</option>
          <option value={8}>8</option>
          <option value={16}>16</option>
          <option value={32}>32</option>
          <option value={64}>64</option>
        </Input>
          </FormGroup>
                <FormGroup className="text-left">
        <Label for="usuarios">Select Multiple</Label>
            <MultiSelect
        options={usuarios}
        value={selected}
        onChange={setSelected}
        labelledBy={"Select"}
      />

      </FormGroup>
        <Button color="success">Crear Juego</Button>
      </Form>
      </Container>
    </div>
  );
}
export default CrearTorneo;
