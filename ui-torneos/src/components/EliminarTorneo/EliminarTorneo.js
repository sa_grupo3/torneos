import React,{useState,useEffect} from 'react';
import './EliminarTorneo.css';
import Header from '../Header/Header';
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

const EliminarTorneo = (props) => {
    const [torneos, setTorneos] = useState([]);
  const [torneo_delete, setTorneo_delete] = useState(0);


  useEffect(() => {
    if (torneos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/torneos")
        .then(res => {
          if (res.data.length == 0) {
            setTorneos([])
          } else {
            setTorneos(res.data)
          }
  })
  .catch(err => {
    console.log(err);
  });
     }
  });


  
  const onSubmit = (e) => {
    e.preventDefault(); 
    console.log(torneo_delete)
    axios.delete(props.TORNEOS_API_HOST + "/torneos/"+torneo_delete)
      .then(res => {
        console.log(res.data);
        axios.get(props.TORNEOS_API_HOST + "/torneos")
        .then(res => {
          if (res.data.length == 0) {
            setTorneos([])
          } else {
            setTorneos(res.data)
          }
  })
      })
      .catch(err => {
        console.log(err);
      });
  }
  return (
    <div className="EliminarTorneo">
      <Header />
      <Container>
        <h1>Eliminar Torneo</h1>
        <Form onSubmit={onSubmit}>
          <FormGroup className="text-left">
            <Label for="torneo">Torneo</Label>
            <Input type="select" name="torneo" id="torneo" onChange={e => setTorneo_delete(e.target.value)}>
              {torneos.map((torneo, key) => {
                return (<option value={torneo._id} key={key}>{torneo.nombre}</option>)
              })}
            </Input>
          </FormGroup>
      
          <Button>Eliminar</Button>
        </Form>
      </Container>
    </div>
  );
}


export default EliminarTorneo;
