import React, { useState } from 'react';
import './CrearJuego.css';
import Header from '../Header/Header';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import axios from 'axios';


async function create(juego) {
  
}

const CrearJuego =  (props) => {
  const [nombre, setNombre] = useState("");
  const [ip, setIp] = useState("");
    const onSubmit = e => {
    e.preventDefault(); 
    const juego = {
      nombre: nombre,
      ip:ip
    }
      if (nombre !== "" && ip !== "") {
        console.log(props.TORNEOS_API_HOST)
        axios.post(props.TORNEOS_API_HOST + "/juegos", juego)
          .then(res => {
            console.log(res)
        alert("Se creo correctamente el juego");  
        
          })
      .catch(err => {
        console.log(err);
      });    
        
    }
      
    
  }
  return (
    <div className="CrearJuego">
      <Header />
      <Container>
      <Form onSubmit={onSubmit}>
        <FormGroup className="text-left">
          <Label for="nombre">Nombre</Label>
          <Input type="text" name="nombre" id="nombre" placeholder="Nombre" value={nombre} onChange={e=>setNombre(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="ip">IP del Juego</Label>
          <Input type="text" name="ip" id="ip" placeholder="IP" value={ip} onChange={e=>setIp(e.target.value)}/>
        </FormGroup>
        <Button color="success">Crear Juego</Button>
      </Form>
      </Container>
    </div>
  )
}

export default CrearJuego;
