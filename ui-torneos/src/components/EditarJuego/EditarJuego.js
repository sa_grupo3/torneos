import React,{useEffect, useState} from 'react';
import './EditarJuego.css';
import Header from '../Header/Header';
import axios from 'axios'
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

const EditarJuego =  (props) => {
  const [juegos, setJuegos] = useState([]);
  const [nombre, setNombre] = useState("");
  const [ip, setIp] = useState("");
  const [id, setId] = useState(0);

  useEffect( () => {
    if (juegos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/juegos")
        .then(res => {
          if (res.data.length == 0) {
            setJuegos([])
          } else { 
          setJuegos(res.data);
        }
        })
    }
  });


   const onSubmit = e => {
    e.preventDefault(); 
    const juego = {
      nombre: nombre,
      ip:ip
    }
   
    axios.put(props.TORNEOS_API_HOST + "/juegos/"+id, juego)
      .then(res => {
        console.log(res.data);
        axios.get(props.TORNEOS_API_HOST + "/juegos")
        .then(res2 => {
          setJuegos(res2.data);
        })
      })
      .catch(err => {
        console.log(err);
      });
  }

  const onChange = (e) => {
    const juego = juegos.filter(j => j._id == e.target.value);
    console.log(juegos)
    if (juego.length == 1) {
      setId(juego[0]._id)
      setNombre(juego[0].nombre);
      setIp(juego[0].ip);
    }
  }


  return (
    <div className="EditarJuego">
      <Header />
      <Container >
       <h1>Editar Juego</h1>
       <Form onSubmit={onSubmit} className="text-left">
         <Label for="juego">Juego</Label>
         <Input type="select" name="juego" id="juego" onChange={onChange}>
              {juegos.map((juego, key) => {
                return (<option value={juego._id} key={key}>{juego.nombre}</option>)
              })}
        </Input>
        <FormGroup className="text-left">
          <Label for="nombre">Nombre</Label>
          <Input type="text" name="nombre" id="nombre" placeholder="Nombre" value={nombre} onChange={e=>setNombre(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="ip">IP</Label>
          <Input type="text" name="ip" id="ip" placeholder="IP" value={ip} onChange={e=>setIp(e.target.value)}/>
        </FormGroup>
        <Button color="success">Actualizar</Button>
      </Form>
    </Container>
    </div>
  );
}

export default EditarJuego;
