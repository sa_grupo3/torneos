import React, { useEffect, useState } from 'react';
import Header from '../Header/Header';
import './EliminarUsuario.css';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';


const EliminarUsuario = (props) => {
  
  const [users, setUsers] = useState([]);
  const [user_delete, setUser_Delete] = useState(0);
  useEffect(() => {
    console.log(users)
    if (users.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/usuarios/jugadores")
        .then(res => {
          if (res.data.length == 0) {
            setUsers([])
          } else {
            setUsers(res.data.usuario)
          }
  })
  .catch(err => {
    console.log(err);
  });
     }
  });
  
  const onSubmit = (e) => {
    e.preventDefault(); 
    console.log(user_delete)
    axios.post(props.TORNEOS_API_HOST + "/usuarios/eliminar", {id:user_delete})
      .then(res => {
        console.log(res.data);
        alert(res.data)
        axios.get(props.TORNEOS_API_HOST + "/usuarios/jugadores")
          .then(res => {
            if (res.data.length == 0) {
              setUsers([])
            } else {
              setUsers(res.data.usuario)
            }
          })
      })
      .catch(err => {
        console.log(err);
      });
  }
  
  return (
    <div className="EliminarUsuario">
      <Header />
      <Container>
        <h1>Eliminar Usuario</h1>
        <Form onSubmit={onSubmit}>
      <FormGroup className="text-left">
        <Label for="exampleSelect">Usuario</Label>
            <Input type="select" name="user" id="user" onChange={e=>setUser_Delete(e.target.value)}>
              {users.map((user, key) => {
                if (user.administrador) return (<div></div>);
                return(<option value={user.id} key={key}>{user.nombres}</option> )}
              )}
        </Input>
      </FormGroup>
      
      <Button>Eliminar</Button>
    </Form>
      </Container>
  </div>);
}


export default EliminarUsuario;
