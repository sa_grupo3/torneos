import React, { useState } from 'react';
import './CrearUsuario.css';
import Header from '../Header/Header';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import axios from 'axios';
const CrearUsuario = (props) => {
  const [correo, setCorreo] = useState("");
  const [password, setPassword] = useState("");
  const [nombres, setNombres] = useState("");
  const [apellidos, setApellidos] = useState("");
  const admin = false;
  const onSubmit = e => {
    e.preventDefault(); 
    const user = {
      id:0,
      email: correo,
      nombres: nombres,
      apellidos: apellidos,
      password: password,
      administrador: admin
    }
    axios.post(props.TORNEOS_API_HOST + "/usuarios/crear", user)
      .then(res => {
        if (res.status == 201) {
          alert("Se creo el usuario correctamente.")
        } else {
          alert("Datos Invalidos");
        }
      })
      .catch(err => {
        alert("Datos Invalidos");
      });
  }
  return (<div>
    <Header />
    <Container >
      <h1>Crear Usuario</h1>
      <Form onSubmit={onSubmit}>
        <FormGroup className="text-left">
          <Label for="correo">Email</Label>
          <Input type="email" name="email" id="correo" placeholder="Email" value={correo} onChange={e=>setCorreo(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="pass">Password</Label>
          <Input type="password" name="password" id="pass" placeholder="Password" value={password} onChange={e=>setPassword(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="nombres">Nombres</Label>
          <Input type="text" name="nombres" id="nombres" placeholder="Nombres" value={nombres} onChange={e=>setNombres(e.target.value)}/>
        </FormGroup>
        <FormGroup className="text-left">
          <Label for="apellidos">Apellidos</Label>
          <Input type="text" name="apellidos" id="apellidos" placeholder="Apellidos" value={apellidos} onChange={e=>setApellidos(e.target.value)}/>
        </FormGroup>
        <Button color="success">Registrar</Button>
      </Form>
    </Container>
  </div>);
}

export default CrearUsuario;
