import React, { useState } from "react";
import { Button, Container, Form, FormGroup, Input, Label,UncontrolledAlert } from "reactstrap";
import "./Login.css";
import { withRouter } from 'react-router-dom'; 
import axios from 'axios';

 function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false)


  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    
    var login = {
      email: email,
      password: password
    }
    axios.post(props.TORNEOS_API_HOST + "/login", login)
      .then(res => {
        if (res.status == 200) {
          setError(false)
          localStorage.setItem("logged",true)
          props.history.push('/Home');      
        } else {
          localStorage.setItem("logged",false)
          setError(true)
        }
      })

  }

  return (
    <div className="Login">
      <Form onSubmit={handleSubmit} className="card card-signin my-5 form-signin">
        <FormGroup id="email" size="large" className="m-4 form-label-group">
          
          <Input
            autoFocus
            type="text"
            id="correo"
            value={email}
            placeholder="user"
            onChange={e => setEmail(e.target.value)}
          />
          <Label for="correo">Usuario</Label>
        </FormGroup>
        <FormGroup id="password" size="large" className="m-4 form-label-group">
          
          <Input
            value={password}
            id="pass"
            onChange={e => setPassword(e.target.value)}
            type="password"
            placeholder="password"
          />
          <Label for="pass">Contraseña</Label>
        </FormGroup>
        
        <div className="text-center">
          {error?<UncontrolledAlert color="danger">Usuario o password incorrecto</UncontrolledAlert>:<div></div>}
        <Button color="primary" size="lg" disabled={!validateForm()} type="submit"  className=" text-uppercase m-4 w-75"  >
          Login
        </Button>
        </div>
        
      </Form>
    </div>
  );
 }
export default withRouter(Login);