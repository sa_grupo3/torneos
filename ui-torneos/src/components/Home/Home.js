import React, { useEffect, useState } from 'react';
import './Home.css';
import Header from '../Header/Header'
import { Container, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';


/*function RenderMenuItem({ dish, onClick }) {
    return (
        <Card>
            <Link to={`/menu/${dish.id}`} >
                <CardImg width="100%" src={baseUrl + dish.image} alt={dish.name} />
                <CardImgOverlay>
                    <CardTitle>{dish.name}</CardTitle>
                </CardImgOverlay>
            </Link>
        </Card>
    );
}*/

const Home = (props) => {
  const [torneos, setTorneos] = useState([]);

  useEffect(() => {
    if (torneos.length == 0) {
      axios.get(props.TORNEOS_API_HOST + "/torneos")
        .then(res => {
          if (res.status == 200) {
            setTorneos(res.data);
          }
        })
      
    }
  })

  const torneosdisp = torneos.map((torneo, key) => {

    console.log(torneo)
        return (
            <tr key={torneo._id}>
            {/* <RenderMenuItem torneo={torneo}  />*/}  
            <td>
              {torneo.nombre}
            </td>
            <td>
              {torneo.juego.nombre}
            </td>
            <td>
              <Link to={`/torneos/${torneo._id}`} >Entrar</Link>
            </td>
           </tr>
        );
  });

  return (
    <div className="Home">
      <Header />
      <Container>
        <h1>Torneos Disponibles</h1>
       <Table responsive striped hover borderless>
        <thead>
          <tr>
                <th>Nombre</th>
                <th>Juego</th>
            <th></th>
            </tr>
        </thead>  
        <tbody>
            {torneosdisp}
        </tbody>
            </Table>
        </Container>
    
    </div>
  )
};

export default Home;
