var express = require('express');
var router = express.Router();

const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const Partida = require('../models/partidas')
const Contador = require('../models/contadorPartida')
const Torneo = require('../models/torneos');
const torneos = require('../models/torneos');
const axios = require('axios')
var publicKey=process.env.PUBLIC_KEY||"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAQA5vqqDW0VcY791GwD5NzII5DPO9cWQfDGu6G/nUUkOFbef1KP9t7MCbqE/fpry5bBnkeaxWp4VzC2Nck0DQ4IuWUC3Uhz9VewCheYgCY/pSGaJoMYJyLk6Bjh98knJ1GtylHsJ+N8JlnVg4FLqyYSFdbV5YGz82iREAYseRMwIDAQAB\n-----END PUBLIC KEY-----"

const URL = process.env.TORNEOS_API_HOST || "http://18.206.38.147:3002";


var token = {}


async function GetToken() {
  var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";

  var token2 = await axios.post(`${token_url}/token?id=craps&secret=manager1`, {})
    .then(function (response) {
      var tk = response.data
      return tk;
})
.catch(function (error) {
  console.log(error);
});
  return token2;
}



/* GET home page. */
router.get('/', function(req, res, next) {
    Partida.find({})
        .then(partida => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(partida);
    })
});

router.get('/:id', (req, res, next) => { 
    
    Partida.findById(req.params.id, (err, partida) => {
        if (err) res.status(500).send(`Error al realizar la peticion: ${err}`)//500 Datos invalidos
        if (!partida) res.status(404).send(`Partida no encontrada: ${err}`)
        res.status(200).send({partida})
    })
})

router.post('/', function (req, res, next) {
    Contador.create({contador:1}).catch((err) => next(err));   
    Contador.count({})
        .then((cont) => {
            var p = req.body;
                p.id = cont;
                
            console.log("No.Partida: " + p.id);

            if (p.jugadores.length != 2) {
                res.status(400).send("Array de jugadores invalido");
            } else {
                Partida.create(p)
                    .then((partida) => {
                        console.log('Partida Creada', partida);
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(partida);
                    }, (err) => next(err))
                    .catch((err) => next(err));                
            }
        })

});




router.put('/:id', function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }
    
    const verifiacion = jwt.verify(token, publicKey, verifyOptions, function (err, verifiacion) {
        if (err) {
            res.status(400).send({ message: "Token invalido" })
        } else {
            console.log("asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasd")
            console.log(req.body)
            console.log(req.params.id)
            let partidaId = req.params.id
            let update = req.body
    

            Partida.findOneAndUpdate({ id: partidaId }, {
                $set: req.body
            }, { new: true })
                .then(partida => {
                    //ya hicimos el put ahora verificar si ya terminaron todas las partidas

                    var idTorneo = ""
                    Torneo.find({}).then(tournaments => {
                        tournaments.forEach(t => {
                            var fases = t.fase;
                            fases.forEach(fase => {
                                fase.llaves.forEach(llave => {
                                    Partida.findById(llave)
                                        .then(async part => {
                                            if (part.id == partidaId) {
                                                idTorneo = t._id
                                                //console.log(fases)
                                        
                                                // Aqui ya tenemos el torneo entonces hay que crear la siguiente fase aca...

                                                verificar_torneo(t._id);
 
                                        
                                            }
                                        });
                                });
                            });
                    
                    
                        });
                    })
            


                    res.statusCode = 201;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(partida);
                }, (err) => next(err))
                .catch((err) => res.status(404).send("Partida No encontrada"));
    
    
        }
    })
});


async function verificar_torneo(id) {
    
    
    Torneo.findById(id).then(async torneo => {
        var fases = torneo.fase;
        var ultima_fase = fases[fases.length - 1];
        var ganadores = await getGanadores(ultima_fase.llaves);
        if (ganadores.length == ultima_fase.llaves.length&& ultima_fase.llaves.length>1 ) {
            // AQUI CREAMOS LA NUEVA FASE

            var noJugadores = ganadores.length;
            var noLlaves = noJugadores / 2;
            var temp_jugadores = [...ganadores];
            console.log("Jugadores",temp_jugadores)
            var new_llaves = [];
            
            //llenar las llaves
            for (var i = 0; i < noLlaves; i++) {
                var llave = [];
                if (temp_jugadores.length > 0) {
                    var jugador1 = temp_jugadores.splice(0, 1); 
                    llave.push(jugador1[0])
                    if (temp_jugadores.length > 0) { 
                        var jugador2 = temp_jugadores.splice(0, 1); 
                        llave.push(jugador2[0])
                    } else {
                        llave.push(-1);
                    }
            }

            //crear las partidas
            var llave_created = await axios.post(URL + "/partidas", {id:torneo.nombre+ noLlaves+"-"+i, jugadores: llave }).catch(err => console.log(err));
                new_llaves.push(llave_created.data._id)
            }

                console.log(torneo.juego.ip)
            var urlTorneo = torneo.juego.ip || process.env.JUEGO_API_HOST;
            

            const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
                axios.post(urlTorneo + "/generar", llave_created.data,config).then(res => {
                    console.log(res.data);
                    console.log(res.status);
                });
                console.log(torneo);
            torneo.fase.push({ nombre: noLlaves, llaves: new_llaves });
                Torneo.findByIdAndUpdate(torneo._id, {
                    $set: torneo
                }, { new: true }).then((new_torneo) => {
                    //console.log(new_torneo)
                    console.log("Nueva Fase Creada")
                });
        }else if (ganadores.length == ultima_fase.llaves.length && ultima_fase.llaves.length==1 ) {
            // AQUI CREAMOS LA NUEVA FASE

            torneo.ganador = ganadores[0];
            torneo.abierto = false;
                Torneo.findByIdAndUpdate(torneo._id, {
                    $set: torneo
                }, { new: true }).then((new_torneo) => {
                    //console.log(new_torneo)
                });
        } else {
            console.log("faltan")
        }
    });
}

async function getGanadores(llaves) {
    var ganadores=[]
    for (let i = 0; i < llaves.length; i++) {
        var partida = await Partida.findById(llaves[i]);
        if (partida.marcador.length == 2) {
            ganadores.push( await esGanador(partida))
        }
    }
    return ganadores;
}

function esGanador(partida) {
        if (partida.marcador[0] > partida.marcador[1]) {      
             return partida.jugadores[0]
                    }else{
            return partida.jugadores[1]
                    }
}



module.exports = router;
