var express = require('express');
var router = express.Router();

var Torneo = require('../models/torneos')
var Partida = require('../models/partidas')
var axios = require('axios')
/* GET home page. */

const URL = process.env.TORNEOS_API_HOST || "http://18.206.38.147:3002";
const URL_Juego = process.env.JUEGO_API_HOST||"http://52.14.140.11:8000";

var token = {}


async function GetToken() {
  var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";

  var token2 = await axios.post(`${token_url}/token?id=craps&secret=manager1`, {})
    .then(function (response) {
      var tk = response.data
      return tk;
})
.catch(function (error) {
  console.log(error);
});
  return token2;
}




router.get('/', function(req, res, next) {
  Torneo.find({})
    .populate('juego')
    .populate('fase.llaves.partida')
    .then((torneo) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(torneo);
  }, (err) => next(err))
            .catch((err) => next(err));
});

router.put("/:id", function (req, res, next) {
    
    Torneo.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
            .then((juego) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(juego);
            }, (err) => next(err))
            .catch((err) => next(err));
})

router.get('/:id', function(req, res, next) {
  Torneo.findById(req.params.id)
    .populate('juego')
    .populate('fase.llaves.partida')
    .then((torneo) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(torneo);
  }, (err) => next(err))
            .catch((err) => next(err));
});

router.post('/', async function (req, res, next) {
  let new_torneo = req.body;
  const copy_jugadores = [...req.body.jugadores];
  var noJugadores = req.body.nojugadores;
  var noLlaves = noJugadores / 2;
  console.log("Numero de llaves: ",noLlaves)

  // TEMPORAL DE JUGADORES
  var temp_jugadores = [...req.body.jugadores];
  console.log("Jugadores",temp_jugadores)
  var llaves = [];

  //llenar las llaves
  for (var i = 0; i < noLlaves; i++) {
    var llave = [];
    if (temp_jugadores.length > 0) {
      var jugador1 = temp_jugadores.splice(Math.floor(Math.random() * temp_jugadores.length), 1); 
      llave.push(jugador1[0])
      if (temp_jugadores.length > 0) { 
        var jugador2 = temp_jugadores.splice(Math.floor(Math.random() * temp_jugadores.length), 1); 
      llave.push(jugador2[0])
      } else {
        llave.push(-1);
      }
    }

    //crear las partidas
    var llave_created = await axios.post(URL + "/partidas", { jugadores: llave }).catch(err => console.log(err));

    console.log(new_torneo.juego.ip)
    var urlTorneo = new_torneo.juego.ip || process.env.JUEGO_API_HOST;
    
          const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };

    axios.post(urlTorneo + "/generar", llave_created.data,config).then(res => {
      console.log(res.data);
      console.log(res.status);
    });
    llaves.push(llave_created.data)
    
  }

  
  
  
  new_torneo.fase = [{ nombre: noLlaves, llaves: llaves }];
  new_torneo.jugadores=copy_jugadores
    Torneo.create(new_torneo)
                    .then((torneo) => {
                        console.log('Torneo Creado', torneo);
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(torneo);
                    }, (err) => console.log(err))
                    .catch((err) => console.log(err));  
});

router.delete('/:id', (req, res, next) => {
Torneo.findByIdAndRemove(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
})

module.exports = router;
