var express = require('express');
var router = express.Router();
var axios = require('axios');
/* GET home page. */

var USUARIOS_HOST = process.env.USUARIOS_HOST || "http://18.206.38.147:5000";
/**
 * Registrar usuarios
 */
var token = {}



async function GetToken() {
  var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";

  var token2 = await axios.post(`${token_url}/token?id=craps&secret=manager1`, {})
    .then(function (response) {
      var tk = response.data
      return tk;
})
.catch(function (error) {
  console.log(error);
});
  return token2;
}


router.post('/crear', async function (req, res, next) {
    var status = 406;
    var new_id = 0;

    const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
  
    axios.post(USUARIOS_HOST + "/jugadores", req.body,config)
        .then(res2 => {
            if (res2.status == 201||res2.status==200) { 
                res.status(201).send(res2.data);
            } else if (res2.statusCode == 406) {
                res.status(406).send("Datos Invalidos");
                status = 406;
            }
        })
        .catch(e => res.status(406).send("Datos Invalidos"));
  
});

/**
 * Editar usuarios
 */


router.post('/editar', async function (req, res, next) {

    console.log(req.body)
    const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
      
  
    var respuesta= await axios.put(USUARIOS_HOST + "/jugadores/"+req.body.id, req.body,config)
        .catch(e => console.log(e));
    if (respuesta.status == 201||respuesta.status==200) {
        res.status(201).send(respuesta.data);
    } else {
     res.status(404).send("Usuario No Encontrado")    
    }
    
});

/**
 * DELETE
 */

router.post('/eliminar',async function (req, res, next) {
    
    console.log(req.body)

    

    var respuesta= await axios.delete(USUARIOS_HOST + "/jugadores/"+req.body.id)
        .catch(e => console.log(e));
    console.log(respuesta.data)
    if(respuesta.status==201||respuesta.status==200)
        res.status(201).send(respuesta.data);
    else res.sendStatus(404);
});

//getJugadores




router.get('/jugadores', async function (req, res, next) {
  const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
  

    const  respuesta =await axios.get(USUARIOS_HOST + "/jugadores", config);
    console.log(respuesta.data)
    if(respuesta.status==201||respuesta.status==200)
        res.status(200).send(respuesta.data);
    else res.sendStatus(404);
});


router.get('/jugadores/:id', async function (req, res, next) {

     const tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
  };
  

    const  respuesta =await axios.get(USUARIOS_HOST + "/jugadores/"+req.params.id,config);
    console.log(respuesta.data)
    if(respuesta.status==201||respuesta.status==200)
        res.status(200).send(respuesta.data);
    else res.sendStatus(404);
});

module.exports = router;
