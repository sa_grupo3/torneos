var express = require('express');
var router = express.Router();
const mongoose = require('mongoose')

const Juegos = require('../models/juegos')


/* GET home page. */

router.post('/', (req, res,next) => {
    console.log(req.body);

    Juegos.create(req.body)
        .then((game) => {
            console.log('Juego Creado ', game);
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(game);
        }, (err) => next(err))
        .catch((err) => next(err));
})

router.get("/", function (req,res,next) {
    Juegos.find({})
        .then((juego) => {
            res.statusCode = 200;
            console.log(juego)
            res.setHeader('Content-Type', 'application/json');
            res.json(juego);
        }, (err) => next(err))
})

router.get("/:id", function (req,res,next) {
    Juegos.findById(req.params.id)
        .then((juego) => {
            res.statusCode = 200;
            console.log(juego)
            res.setHeader('Content-Type', 'application/json');
            res.json(juego);
        }, (err) => next(err))
})



router.put("/:id", function (req, res, next) {
    Juegos.findByIdAndUpdate(req.params.id, {
            $set: req.body
        }, { new: true })
            .then((juego) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(juego);
            }, (err) => next(err))
            .catch((err) => next(err));
})

router.delete("/:id", function (req, res, next) {
    
    Juegos.findByIdAndRemove(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
})


module.exports = router;
