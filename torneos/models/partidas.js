const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const partidaSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    jugadores: [Number],
    marcador: [Number],
});


module.exports = mongoose.model('Partida', partidaSchema);