const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contadorpartidaSchema = new Schema({
    contador: {
        type:Number
    }
});


module.exports = mongoose.model('ContadorPartida', contadorpartidaSchema);