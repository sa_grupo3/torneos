'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const JuegoSchema = Schema({
    nombre: String,
    ip: String
})

module.exports = mongoose.model('Juego', JuegoSchema)