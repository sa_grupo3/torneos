const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const faseSchema = new Schema({
    nombre: {
        type:String
    },
    llaves:[{ type: Schema.Types.ObjectId, ref: 'Partida' }]
});

const torneosSchema = new Schema({
    nombre: {
        type:String
    },
    juego: { type: Schema.Types.ObjectId, ref: 'Juego' },
    nojugadores: {
        type: Number
    },
    jugadores: [Number],
    fase: [faseSchema],
    abierto: { type: Boolean },
    ganador: { type: Number }
    
});


module.exports = mongoose.model('Torneo', torneosSchema);