var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var torneosRouter = require('./routes/torneos');
var usuariosRouter = require('./routes/usuarios');
var loginRouter = require('./routes/login')
var juegosRouter = require('./routes/juegos');
var partidasRouter = require('./routes/partidas');

var cors = require('cors');

const mongoose = require('mongoose');

const url = "mongodb://18.206.38.147:27017/torneos";
const connect = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

connect.then(() => {
  console.log("Connected correctly to server");
}, (err) => { console.log(err); });






var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/torneos', torneosRouter);
app.use('/usuarios', usuariosRouter);
app.use('/login', loginRouter);
app.use('/juegos', juegosRouter);
app.use('/partidas', partidasRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
