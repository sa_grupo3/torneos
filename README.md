# Microservicio Torneos

Microservicio encargado de crear juegos, usuarios y torneos.

## Instalación

    Utilizar el administrador de paquetes [npm](https://www.npmjs.com) para instalar las dependencias.
    Es necesario tener docker y docker-compose instalado

## Iniciar

```bash
docker-compose build
docker-compose up -d
```

## Uso

### /PUT

`PUT /partidas/{id}`

    http://localhost:3002/partidas/10

`body`

    { "marcador": [1300,600] }

### Respuesta

    HTTP/1.1 201 Created
    Date: Thu, 29 Oct 2020 12:36:30 GMT
    Status: 201 Created
    Connection: closed
    Content-Type: application/json
    Content-Length: W/"11-/7jGa7JrZgdGWNMafVESBNGfxfc"
